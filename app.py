from flask import Flask
from flask_restplus import Api


# main flask application instance
app = Flask(__name__)
env = app.config['ENV']
app.config.from_object('config.{}'.format(env))
settings = app.config
api = Api(app)

# registers all the blueprint and routes of each version of api
from apis.v1 import routes

if __name__ == '__main__':
    app.run(debug=settings['DEBUG'])
