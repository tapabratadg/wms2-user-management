import csv
import json
import base64
import requests
import StringIO


def lambda_handler(event, context):
    """
    Description
        lambda to handle files and convert them to json format
    Args
        entity: uploaded for which entity
        file: 
    """
    print(event)
    data = event['body-json']
    print("encoded file {}".format(data))
    file_data = base64.b64decode(data)
    print("decoded file {}".format(file_data))
    result = csv.DictReader(file_data.splitlines())
    json_converted = [entry for entry in result]
    print("final json body {}".format(json_converted))
    
    if event['querystring']['entity'] == 'product':
        product_upload(json_converted, event['querystring']['client_id'])
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
    
def product_upload(payload, client_id):
    url = 'http://52.77.57.70:5004/api/v1/product/bulk/{}/'
    payload = {'products': payload}
    r = requests.post(url = url.format(client_id), data = payload)
    print("response from product upload api {}".format(r))